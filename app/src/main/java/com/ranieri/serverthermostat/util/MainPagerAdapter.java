package com.ranieri.serverthermostat.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ranieri.serverthermostat.R;
import com.ranieri.serverthermostat.fragment.ChartFragment;
import com.ranieri.serverthermostat.fragment.GaugeFragment;

/**
 * Created by ranie on 26 de dez.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    private int mNumOfTabs;
    private Context mContext;
    private GaugeFragment mGaugeFragment;
    private ChartFragment mChartFragment;

    public MainPagerAdapter(Context context, FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mContext = context;
        mGaugeFragment = new GaugeFragment();
        mChartFragment = new ChartFragment();
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return mGaugeFragment;
            case 1:
                return mChartFragment;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getResources().getString(R.string.current_temperature);
            case 1:
                return mContext.getResources().getString(R.string.temperature_history);
            default:
                return null;
        }
    }
}
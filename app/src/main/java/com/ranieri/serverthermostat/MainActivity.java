package com.ranieri.serverthermostat;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.github.mikephil.charting.data.Entry;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.ranieri.serverthermostat.fragment.ChartFragment;
import com.ranieri.serverthermostat.fragment.GaugeFragment;
import com.ranieri.serverthermostat.util.MainPagerAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    // Swipe View
    MainPagerAdapter mainPagerAdapter;
    ViewPager mViewPager;
    GaugeFragment mGaugeFragment;
    ChartFragment mChartFragment;

    int tempQueryLimit = 240;

    // Firebase
    FirebaseDatabase mDatabase;
    DatabaseReference mDatabaseReference;
    ValueEventListener eventListener;
    Query recentTempQuery;
    ArrayList<Entry> entries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_pager);
        ButterKnife.bind(this);

        // Swipe View
        mainPagerAdapter = new MainPagerAdapter(MainActivity.this, getSupportFragmentManager(), 2);
        mGaugeFragment = (GaugeFragment) mainPagerAdapter.getItem(0);
        mChartFragment = (ChartFragment) mainPagerAdapter.getItem(1);
        mViewPager = findViewById(R.id.pager);
        mViewPager.setAdapter(mainPagerAdapter);

        // Firebase
        mDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mDatabase.getReference().child("tempHistory");
        recentTempQuery = mDatabaseReference.limitToLast(tempQueryLimit);
    }

    @Override
    protected void onStart() {
        super.onStart();
        attachDatabaseReadListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        detachDatabaseReadListener();
    }

    // Firebase

    private void detachDatabaseReadListener() {
        if (eventListener != null) {
            mDatabaseReference.removeEventListener(eventListener);
            eventListener = null;
        }
    }

    private void attachDatabaseReadListener() {
        if (eventListener == null) {
            eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    float minValue = Float.MAX_VALUE, maxValue = Float.MIN_VALUE, minKey = Float.MAX_VALUE, maxKey = Float.MIN_VALUE, key;
                    Float value = 0f;
                    entries = new ArrayList<>();

                    for (DataSnapshot tempSnapshot : dataSnapshot.getChildren()) {
                        value = tempSnapshot.child("temp").getValue(Float.class);
                        key = Float.parseFloat(tempSnapshot.getKey());

                        minValue = Math.min(minValue, value);
                        maxValue = Math.max(maxValue, value);
                        minKey = Math.min(minKey, key);
                        maxKey = Math.max(maxKey, key);

                        entries.add(new Entry(key, value));
                    }
                    //Toast.makeText(MainActivity.this, "onDataChange" + temp, Toast.LENGTH_LONG).show();
                    //Toast.makeText(MainActivity.this, "minValue: " + minValue + " maxValue: " + maxValue + " minKey:" + minKey + " maxKey: " + maxKey, Toast.LENGTH_LONG).show();

                    if (mGaugeFragment != null) {
                        UpdateGaugeData listener = mGaugeFragment;
                        listener.setupGauger(value, Math.round(minValue), Math.round(maxValue));
                    }

                    if (mChartFragment != null) {
                        UpdateChartData listener = mChartFragment;
                        listener.setValueToChart(entries, minValue, maxValue, minKey, maxKey);
                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    Toast.makeText(MainActivity.this, "Failed to read value.", Toast.LENGTH_LONG).show();
                }
            };
            recentTempQuery.addValueEventListener(eventListener);
        }
    }

    public interface UpdateGaugeData {
        void setupGauger(float tempValue, int minTemp, int maxTemp);
    }

    public interface UpdateChartData {
        void setValueToChart(ArrayList<Entry> entries, float minY, float maxY, float minX, float maxX);
    }
}